import breeze.numerics.sqrt

class Point2D (private val x: Float, private val y: Float) {

  def getX = {this.x}
  def getY = {this.y}

  def distance (pt2: Point2D) = sqrt( utils.power(this.x - pt2.x, 2) + utils.power(this.y - pt2.y, 2) )

}

object utils {

  def power(x: Float, p: Int): Float = if (p == 0) 1 else x * power(x, p-1)

  def min2 (x: Float, y: Float) = if (x < y) x else y

}