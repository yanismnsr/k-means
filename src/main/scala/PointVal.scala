class PointVal (private val x: Float, private val y: Float) extends Point2D (x, y) {

  private var ptK: PointK = _

  def pointK = {this.ptK};

  def pointK_= (newMean : PointK) {this.ptK = newMean}

  def minimize (kMeansList: List[PointK]) = {
    val distances = kMeansList.map(pt => (pt, this.distance(pt)))
    /*println(distances.min(new Ordering[(Point2D, Float)] {
      def compare(x: (Point2D, Float), y: (Point2D, Float)) = if (x._2 < y._2) x._2 else y._2
    }))*/
    println(distances.reduceLeft((a, b) => if (a._2 < b._2) a else b)._2)
  }

}
